//
//  PlayerView.swift
//  HandRemote
//
//  Created by Christian Varriale on 21/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PlayerView: UIView{
    
    //MARK: - Properties
    
    //Player con layer
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
    //MARK: - Function
    
    //Prendo un URL ma potrei prendere anche elementi della Galleria
    func setPlayerURL(url: URL) {
        player = AVPlayer(url: url)
        player.allowsExternalPlayback = true    //così da usare AirPlay
        
        playerLayer = AVPlayerLayer(player: player)     //inizializzo il playerLayer
        playerLayer.videoGravity = .resizeAspectFill
        
        self.layer.addSublayer(playerLayer)     //così da vedere qualcosa, ereditando layer da UIView
        playerLayer.frame = self.bounds         //resize del frame
    }
}
