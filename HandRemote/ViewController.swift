//
//  ViewController.swift
//  HandRemote
//
//  Created by Christian Varriale on 21/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Vision

enum RemoteCommand: String{
    case none
    case fist = "Fist"
    case open = "Open"
}

class ViewController: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var handImageView: UIImageView!
    @IBOutlet weak var playerView: PlayerView!
    
    //MARK: - Properties Camera and Vision
    //Creo Sessione, sulla quale il Device si baserà. Si imposta la posizione.
    let captureSession = AVCaptureSession()
    var captureDevice: AVCaptureDevice!
    var devicePosition: AVCaptureDevice.Position = .front
    
    //Creo un vettore di richieste
    var requests = [VNRequest]()
    
    //To avoid bad command
    let bufferSize = 3
    var commandBuffer = [RemoteCommand]()
    var currentCommand: RemoteCommand = .none{
        didSet{
            //Cambiando la variabile, faccio l'append nel vettore
            commandBuffer.append(currentCommand)
            
            // Se il buffer si è riempito, controllo che ogni elemento del buffer sia uguale a quello attuale
            if commandBuffer.count == bufferSize{
                
                if commandBuffer.filter({$0 == currentCommand}).count == bufferSize{
                    //Send Command
                    showAndSendCommand(command: currentCommand)
                }
                commandBuffer.removeAll()
            }
        }
    }
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVision()
        setupPlayer()
        setupAirPlay()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareCamera()
    }
    
    //MARK: - Function
    func setupVision(){
        guard let visionModel = try? VNCoreMLModel(for: HandClassifier().model) else {
            fatalError("Errore nel caricamento del modello")
        }
        
        //Questa requesta sarà diversa da quella utilizzata nel request handler, serve solo per la Observation
        let classificationRequest = VNCoreMLRequest(model: visionModel, completionHandler: self.handleClassification)
        
        classificationRequest.imageCropAndScaleOption = .centerCrop
        
        //Aggiungo la richiesta (o le richieste) al vettore
        self.requests = [classificationRequest]
    }
    
    //Qui streammo il Video
    func setupPlayer(){
        playerView.setPlayerURL(url: URL(string: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")!)
        
        playerView.player.play()
    }
    
    //Qui gestisco l'AirPlay
    func setupAirPlay(){
        //create AVRoutePickerView
        
        let airPlay = AVRoutePickerView(frame: CGRect(x: 0, y: 40, width: 80, height: 80))
        
        airPlay.center.x = self.view.center.x
        
        airPlay.tintColor = UIColor.white
        
        self.view.addSubview(airPlay)
    }
    
    //MARK: - Handler
    func handleClassification(request: VNRequest, error:Error?){
        guard let observation = request.results else {
            print("No Result")
            return
        }
        
        let classifications = observation
            .compactMap({$0 as? VNClassificationObservation})
            .filter({$0.confidence > 0.5})
            .map({$0.identifier})
        
        print(classifications)
        
        //ogni volta che c'è una classification, la si controlla e si cambia il current command definito nel didSet
        switch classifications.first {
        case "None":
            currentCommand = .none
        case "Fist":
            currentCommand = .fist
        case "Open":
            currentCommand = .open
        default:
            currentCommand = .none
        }
    }
    
    //MARK: - SendCommand
    func showAndSendCommand(command:RemoteCommand){
        
        DispatchQueue.main.async {
            if command == .open{
                self.playerView.player.play()
                self.handImageView.image = UIImage(named: command.rawValue)
            }else if command == .fist{
                self.playerView.player.pause()
                self.handImageView.image = UIImage(named: command.rawValue)
            }
        }
    }
}

