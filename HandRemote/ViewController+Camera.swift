//
//  ViewController+Camera.swift
//  HandRemote
//
//  Created by Christian Varriale on 21/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import AVFoundation
import Vision

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate{
    
    //imposto la fotocamera sulla parte frontale, in modalità video sfruttando come dispositivo la camera built-in
    func prepareCamera(){
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .front).devices
        
        captureDevice = availableDevices.first
        
        beginSession()
    }
    
    func beginSession(){
        //Facciamo partire la sessione individuando se c'è qualche dispositivo disponibile, lo aggiungo alla session, che poi configurerò.
        
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureDeviceInput)
        } catch {
            fatalError("Couldn't create video device input")
        }
        
        captureSession.beginConfiguration()
        captureSession.sessionPreset = .vga640x480
        
        //creazione dell'output per Vision
        let dataOutput = AVCaptureVideoDataOutput()
        
        //Settings di Compressione
        dataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
        
        dataOutput.alwaysDiscardsLateVideoFrames = true
        
        //Aggiunta Output alla sessione
        if captureSession.canAddOutput(dataOutput){
            captureSession.addOutput(dataOutput)
        }
        
        captureSession.commitConfiguration()
        
        //Connessione con Visione attraverso il bufferDelegate
        let queue = DispatchQueue(label: "captureQueue")
        dataOutput.setSampleBufferDelegate(self, queue: queue)
        
        //Inizio a prendere le info dalla camera
        captureSession.startRunning()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        //Prendo l'immagine catturata dalla camera per convertirla nel pixelBuffer da passare in Vision
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        
        //Servono anche delle info di orientamento
        let exifOrientation = self.exifOrientationFromDeviceOrientation()
        
        //creo la richiesta, che si prende il buffer, l'orientation ed opzioni nulle
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer, orientation: exifOrientation, options: [:])
        
        do {
            try imageRequestHandler.perform(self.requests)
        } catch {
            print(error)
        }
        
    }
    
    func exifOrientationFromDeviceOrientation() -> CGImagePropertyOrientation {
        let curDeviceOrientation = UIDevice.current.orientation
        let exifOrientation: CGImagePropertyOrientation
        
        switch curDeviceOrientation {
            case UIDeviceOrientation.portraitUpsideDown:    //Device Oriented Vertically, Home Button on the Top
                exifOrientation = .left
            case UIDeviceOrientation.landscapeLeft:         //Device Oriented Horizontally, Home Button on the Right
                exifOrientation = .upMirrored
            case UIDeviceOrientation.landscapeRight:        //Device Oriented Horizontally, Home Button on the Left
                exifOrientation = .down
            case UIDeviceOrientation.portrait:              //Device Oriented Vertically, Home Button on the Bottom
                exifOrientation = .up
        default:
                exifOrientation = .up
        }
        
        return exifOrientation
    }
    
}
